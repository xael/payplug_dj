#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_payplug_dj
------------

Tests for `payplug_dj` models module.
"""

from django.test import TestCase

from payplug_dj.models import Payment

from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
import time
from selenium import webdriver


class TestPayplug_dj(TestCase):

    def setUp(self):
        Payment.objects.create(
            email='2016@xael.org',
            firstname='Alexandre',
            lastname='Norman',
            amount=42.24,
            currency='EUR',
        )
        p = Payment.objects.get(email='2016@xael.org')
        p.payplug_metadata = {
            'my_invoice_id': 'Invoice-03',
        }
        p.save()

        self.browser = webdriver.Firefox()
        self.browser.maximize_window()
        self.browser.implicitly_wait(10)

        Site.objects.get_current().domain = 'localhost:8000'
        return

    def test_create_payment(self):
        """test creating payment"""
        p = Payment.objects.get(email='2016@xael.org')
        p.create_payment()

        self.assertNotEqual(
            '',
            p.payplug_id
        )
        self.assertNotEqual(
            '',
            p.payplug_url
        )

        p = Payment.objects.get(email='2016@xael.org')
        self.assertNotEqual(
            '',
            p.payplug_url
            )
        self.browser.get(p.payplug_url)

        self.browser.find_element_by_id('paymentCardNumber').send_keys('4242424242424242')
        self.browser.find_element_by_id('paymentCardExpiration').send_keys('1299')
        self.browser.find_element_by_id('paymentCardCvv').send_keys('123')

        # next page
        self.browser.find_element_by_id('payButton').click()
        self.browser.find_element_by_id('returnToMerchantSiteText').click()
        # time.sleep(5)

        self.assertIn(
            str(p.uuid),
            self.browser.current_url
        )

        time.sleep(5)
        self.assertEquals(
            ''.join([
                'http://',
                Site.objects.get_current().domain,
                reverse(
                    'payplug_dj:return',
                    kwargs={'uuid': str(p.uuid)}
                )]),
            self.browser.current_url
        )
        return

    def test_create_payment_and_cancel(self):
        """test creating payment and cancel"""
        p = Payment.objects.get(email='2016@xael.org')
        p.create_payment()

        self.assertNotEqual(
            '',
            p.payplug_id
        )
        self.assertNotEqual(
            '',
            p.payplug_url
        )

        p = Payment.objects.get(email='2016@xael.org')
        self.assertNotEqual(
            '',
            p.payplug_url
            )
        self.browser.get(p.payplug_url)

        # next page
        self.browser.find_element_by_id('linkBackMerchant').click()

        self.assertIn(
            str(p.uuid),
            self.browser.current_url
        )

        self.assertEquals(
            ''.join([
                'http://',
                Site.objects.get_current().domain,
                reverse(
                    'payplug_dj:cancel',
                    kwargs={'uuid': str(p.uuid)}
                )]),
            self.browser.current_url
        )
        return

    def tearDown(self):
        self.browser.quit()
        return
