=======
Credits
=======

Development Lead
----------------

* Alexandre Norman <norman@xael.org>

Contributors
------------

None yet. Why not be the first?
